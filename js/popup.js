var p = new Promise(function (resolve, reject) {
	var dataSet = true;
	chrome.storage.sync.get('dataSet', function (items) {
			dataSet = items['dataSet'];
			resolve(dataSet);
	});
});

function dateObjectToCountdown(dateString) {
	let is_expired = false;
	today = new Date();
	const [day, month, year] = dateString.split('/');
	eventDate = new Date(`${year}-${month}-${day}`);
	todayMoment = moment();
	eventMoment = moment(eventDate);

	const diff = moment.preciseDiff(todayMoment, eventMoment, true);
	const firstDateWasLater = diff['firstDateWasLater'];
	const yearsText = diff['years'] == '1' ? 'year' : 'years';
	const years = diff['years'] ? `<div class='countdownItem'>${diff['years']}<span>${yearsText}</span></div>` : '';
	const monthsText = diff['months'] == '1' ? 'month' : 'months';
	const months = diff['months'] ? `<div class='countdownItem'>${diff['months']}<span>${monthsText}</span></div>` : '';
	const daysText = diff['days'] == '1' ? 'day' : 'days';
	const days = diff['days'] ? `<div class='countdownItem'>${diff['days']}<span>${daysText}</span></div>` : '';
	const hoursText = diff['hours'] == '1' ? 'hour' : 'hours';
	const hours = diff['hours'] ? `<div class='countdownItem'>${diff['hours']}<span>${hoursText}</span></div>` : '';
	const minutesText = diff['minutes'] == '1' ? 'minute' : 'minutes';
	const minutes = diff['minutes'] ? `<div class='countdownItem'>${diff['minutes']}<span>${minutesText}</span></div>` : '';
	const secondsText = diff['seconds'] == '1' ? 'second' : 'seconds';
	const seconds = diff['seconds'] ? `<div class='countdownItem'>${diff['seconds']}<span>${secondsText}</span></div>` : '';

	if (is_expired) {
		return todayMoment > eventMoment ? 'expired' : '';
	}
	return `<div class="countdownItemContainer">${years} ${months} ${days} ${hours} ${minutes} ${seconds}</div>`;
}
function loadData(dataArray) {
	const ordered = dataArray.sort(function(a, b) {
		const [day_a, month_a, year_a] = a.eventDate.split('/');
		const [day_b, month_b, year_b] = b.eventDate.split('/');
		return new Date(`${year_a}-${month_a}-${day_a}`) - new Date(`${year_b}-${month_b}-${day_b}`);
	});
	const html = ordered.map(element =>{
		const [day, month, year] = element.eventDate.split('/');
		eventDate = new Date(`${year}-${month}-${day}`);
		var months = ["january","february","march","april","may","june","july","august","september","november","december"];

		return `
		<div class="card bg-dark text-white" data-eventTime="${element.eventDate}">
			<div class="card-image-container">
				<img class="card-img-top" src="${!element.eventImage ? 'http://www.google.nl/' : element.eventImage}" alt="Card image">
			</div>
			<div class="card-body">
				<h6 class="card-title">${element.eventName}</h6>
				<p class="card-text description">${element.eventDescription}</p>
				<p class="card-text">
					Category : ${element.eventCategory}<br />
					Date : ${months[eventDate.getMonth()-1]} ${eventDate.getDate()}, ${eventDate.getFullYear()}<br />

				</p>
			</div>
		</div>`;
	}).join('');
	// <div class="timer" data-time="${(element.eventDate.split('/')[2])}-${(element.eventDate.split('/')[1])}-${(element.eventDate.split('/')[0])}">${dateObjectToCountdown(element.eventDate)}</div>
	const events = document.getElementById('events');
	events.innerHTML = html;
}
window.onload = function() {
	p.then(function (dataSet) {
		dataSet ? loadData(dataSet) : console.log('no events to load');
	});
	p.then(function (dataSet) {
		
	});
}