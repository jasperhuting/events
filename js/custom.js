var lp = lp || {
	events: [],
	categories: [],
	amountEvents: 0,
	errors: 0,
	catColors: {
		sport: ["#20c997"], // teal
		television: ["#ffc107"], // yellow
		music: ["#fd7e14"], // orange
		personal: ["#dc3545"] // red
		// Festival: ["#e83e8c"] // pink
	},
	teams: [],
	standing: [],
	fixtures: [],
	roundsArray: [],
	competition: 464, // 449 is eredivisie // 464 is champions league
	fillCategories: function() {
		chrome.storage.sync.get("categories", function(dataObj) {
			console.log(dataObj);
			if (typeof dataObj["categories"] !== "undefined") {
				lp.categories = dataObj["categories"];
				var amountCategories = Object.keys(lp.categories).length;
				$("#categoriesBadge").html(amountCategories);

				var options = [];
				for (var key in  lp.categories) {

					options.push({
						value:  lp.categories[key].id,
						name:  lp.categories[key].name
					});

				lp.fillCategoryList(lp.categories);
				}//eo for loop categories

				Promise.all(options).then(function () {
					console.log('ready');
					$("#eventCategory").selectize({
						maxItems: 1,
						valueField: "value",
						labelField: "name",
						searchField: ["name"],
						placeholder: "choose category",
						options: options,
						render: {
							item: function(item, escape) {
								return (
									`<div style="color:${lp.catColors[item.name]}">${(item.name? '<span class="name">' + item.name + '</span>': '')}</div>`
								);
							},
							option: function(item, escape) {
								const label = item.name || item.value;
								return (
									`<div style="color:${lp.catColors[item.name]}"><span class="label">${label}</span></div>`
								);
							}
						},
						onChange(value) {
							document.getElementById('container_eventExtra_imdb').children[0].classList.add('d-none');
							document.getElementById('container_eventExtra_music').children[0].classList.add('d-none');
							document.getElementById('container_eventExtra_soccer').children[0].classList.add('d-none');
							let color = value ? `${lp.catColors[options[value].name]}` : "#17a2b8";
							this.$control[0].style.boxShadow = `${color} 0px 0px 5px`;
							this.$control[0].style.border = `1px solid ${color}`;
							$(":root").css("--main-color", `${color}`);

							// sport
							if (value === "0") {
								document.querySelector("label[name='eventExtra_soccer']").textContent = "Search soccer matches";
								document.getElementById('container_eventExtra_soccer').children[0].classList.remove('d-none');
								lf.getCompetitions();
							}
							// music
							if (value === "1") {
								document.getElementById('container_eventExtra_music').children[0].classList.remove('d-none');
								document.querySelector("label[name='eventExtra_music_artists']").textContent = "Search Artists";
								lf.setMusicSearch();
							}
							// television
							if (value === "2") {
								document.getElementById('container_eventExtra_imdb').children[0].classList.remove('d-none');
								document.querySelector("label[name='eventExtra_imdb']").textContent = "Search on movie/serie/game";
								lf.setImdbSearch();
							}

						},
						onBlur(value) {
							this.onChange(value);
						},
						onDropdownOpen(dropdown) {
							this.clear();
						}
					}); //eo selectize
				}); //eo promise
			} //eo check if categories != undefined


		});
	},
	fillCategoryList: function(categoriesObject) {
		$("#categories tbody").html('');
		for (key in categoriesObject) {
			$("#categories tbody").append(`
							<tr data-index="${categoriesObject[key].id}">
								<th scope="row">${categoriesObject[key].id}</th>
								<td><span data-category="cat_${categoriesObject[key].name}">${categoriesObject[key].name}</span></td>
								<td>${categoriesObject[key].extraFields}</td>
							</tr>`);
		}//eo for loop
	},
	fillEditList: async function(dataArray) {
		function fillEditList() {
			$("#entries tbody").html("");
			for (key in dataArray) {
				var catName = "none";
				if (dataArray[key].eventCategory > -1) {
					catName = lp.categories[dataArray[key].eventCategory].name;
				}
				$("#entries tbody").append(`
							<tr data-index="${key}">
							<th scope="row">${dataArray[key].uniqueId}</th>
							<td>${dataArray[key].eventName}</td>
							<td>${dataArray[key].eventDate} - ${dataArray[key].eventTime ? dataArray[key].eventTime : "00:00"}</td>
							<td><span class="btn" data-category="cat_${catName}">${catName}</span></td>
							<td><div class="badge badge-light text-danger border border-danger delete" data-id="${key}">x</div></td>
							</tr>
					`);
			}
		}
		function setColor() {
			for (var i = 0; i < $("span[data-category]").length; i++) {
				var catName = $($("span[data-category]")[i])
					.data("category")
					.split("_")[1];
				$($("span[data-category]")[i]).css({
					border: "1px solid " + lp.catColors[catName][0],
					color: lp.catColors[catName][0]
				});
			}
		}
		await fillEditList();
		await setColor();
	},

	resetFeedbackFields: function(form) {
		$("#" + form.id + " .invalid-feedback").html("").hide();
		$("#" + form.id + " *").removeClass("is-invalid");
	},

};
// eo lp

$(document).ready(function() {
	$(".time").mask("00:00");
	$(".date").mask("00/00/0000");

	lp.fillCategories(); // get all the categories
	lf.getEvents(); // get all the events

	// click events
	$("#entries").on("click", ".delete", function (event) {

		const clickedElem = this;
		const clickedIndex = this.dataset.id;
		const arrayIndex = lp.events.indexOf(lp.events[clickedIndex]);
		lp.events.splice(arrayIndex,1);

		chrome.storage.sync.set({ dataSet: lp.events }, function() {
			lf.checkIfEvents().then(function(val) {
				events = val.dataSet;
				lp.events = events;
				lp.fillEditList(events);
			});
		});

	});
	function orderEventList(order, reverse) {
		lf.checkIfEvents().then(function(val) {
			events = val.dataSet;

			switch (order) {
				case 'id':
					lp.events = events.sort((a,b) => a.uniqueId > b.uniqueId ? 1 : -1);
					if (reverse) {
						lp.events = events.sort((a,b) => a.uniqueId < b.uniqueId ? 1 : -1);
					}
					break;
				case 'name':
					lp.events = events.sort((a,b) => a.eventName > b.eventName ? 1 : -1);
					if (reverse) {
						lp.events = events.sort((a,b) => a.eventName < b.eventName ? 1 : -1);
					}
					break;
				case 'date':
					lp.events = events.sort(function(a, b) {
						const [day_a, month_a, year_a] = a.eventDate.split('/');
						const [day_b, month_b, year_b] = b.eventDate.split('/');
						return new Date(`${year_a}-${month_a}-${day_a}`) - new Date(`${year_b}-${month_b}-${day_b}`);
					});
					if (reverse) {
						lp.events = events.sort(function(a, b) {
							const [day_a, month_a, year_a] = a.eventDate.split('/');
							const [day_b, month_b, year_b] = b.eventDate.split('/');
							return new Date(`${year_b}-${month_b}-${day_b}`) - new Date(`${year_a}-${month_a}-${day_a}`);
						});
					}
					break;
				case 'category':
					lp.events = events.sort((a,b) => a.eventCategory > b.eventCategory ? 1 : -1);
					if (reverse) {
						lp.events = events.sort((a,b) => a.eventCategory < b.eventCategory ? 1 : -1);
					}
					break;
				default:
					lp.events = events;
					break;
			}
			lp.fillEditList(events);
		});

		event.stopPropagation();

	}
	$("#entries").on("click", "th", function(event) {
		event.stopPropagation();
		const reverse = (this.dataset.reverse === "true");
		this.dataset.reverse = !reverse;
		orderEventList(this.dataset.order, !reverse);
	});
	$("#entries").on("click", "tr", function(event) {
		if (!event.target.classList.contains('delete')) {
			$("a[href='#add']").click();
		lf.populateForm("#eventAdd", lp.events[this.dataset["index"]]);
		var catId = lp.events[this.dataset["index"]].eventCategory;
		$("#eventCategory").selectize()[0].selectize.setValue(catId);
		}
	});

	// eventListeners
	document.getElementById("eventAdd").addEventListener('submit', function(event) {
		event.preventDefault();

		let formElements = event.currentTarget.elements;
		const formElementsArray = Array.from(formElements)
		const checkElements = formElementsArray
			.filter(elem => Object.keys(elem.dataset).length) // check if input has data items (if not it wont be checked)
			.filter(elem => elem.dataset.check == "true") // check if data-check == "true"
			.filter(elem => (elem.dataset.required == "false" && elem.value.length > 0) || (elem.dataset.required =="true")); // if required is false and its empty they dont need to be checked
		let errors = false;
		function checkElems(elements) {

			return new Promise(function(resolve, reject) {

				elements.forEach(elem => {
					if (elem.dataset.extracheckcriterion) {
						switch (elem.dataset.extracheckcriterion) {
							case "url":
								if (!lf.isURL(elem.value)) {
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').style.display = 'block';
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').textContent = "Not a valid url";
									errors = true;
								}
								break;
							case "date":
								if (!lf.isDate(elem.value)) {
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').style.display = 'block';
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').textContent = "Not a valid date, date must be in the future";
									event.currentTarget.querySelector('[name="'+elem.name+'"]').classList.replace('valid', 'is-invalid');
									errors = true;
								}
								break;
							case "time":
								if (!lf.isTime(elem.value)) {
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').style.display = 'block';
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').textContent = "Not a valid time";
									event.currentTarget.querySelector('[name="'+elem.name+'"]').classList.replace('valid', 'is-invalid');
									errors = true;
								}
								break;
							case "imdb":
								if (!lf.isImdb(elem.value)) {
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').style.display = 'block';
									event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').textContent = "Not a valid imdb code";
									event.currentTarget.querySelector('[name="'+elem.name+'"]').classList.replace('valid', 'is-invalid');
									errors = true;
								}
								break;
							default:
								break;
						}
					} // check criterions

					if (elem.dataset.required == "true" && elem.value.length <= 0) {
						event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').style.display = 'block';
						event.currentTarget.querySelector('[name="'+elem.name+'_feedback"]').textContent = "This field is required";
						event.currentTarget.querySelector('[name="'+elem.name+'"]').classList.replace('valid', 'is-invalid');
						errors = true;
					}

				}); // eo for each elements

				resolve(errors);
			});
		}

		checkElems(checkElements).then(function(errors) {
			if (!errors) {
				let events = [];
				lf.checkIfEvents().then(function(val) {
					events = val.dataSet;
					lp.events = events;

					const uniqueId = formElementsArray.filter(elem => elem.name == "uniqueId")[0].value;

					function checkIdDataSet(uniqueId, events) {
						if (isNaN(uniqueId) && uniqueId.length > 0) { // check if uniqueId is a number
							console.log('klopt dit?');
							return false
						}
						const uniqueDataSetId = events.filter(elem => elem.uniqueId == uniqueId);
						if (!uniqueDataSetId) { // check if uniqueId is present on dataSet
							console.log('en dit?');
							return false
						}
						return uniqueDataSetId;
					}
					// check if submit = edit or new
					function save(id) {
						newObject = {};
						formElementsArray.forEach(element => {
							if (element.type !== "submit" && element.type !== "reset" && element.name) {
								let name = element.name;
								newObject[name] = element.value;
							}
						});
						newObject['uniqueId'] = id;
						events[id] = newObject;
						lp.events = events;
						chrome.storage.sync.set({ dataSet: events }, function() {
							lf.checkIfEvents().then(function(val) {
								events = val.dataSet;
								lp.events = events;
								lp.fillEditList(events);
								$("#eventAdd")[0].reset();
							});
						});
					}
					if (formElementsArray.filter(elem => elem.name == "uniqueId")[0].value.length > 0) {
						const dataSetId = checkIdDataSet(uniqueId, events);
						save(events.findIndex(elem => elem.uniqueId == uniqueId));
					} else { // if new
						save(events.length);
					} //eo edit
					lf.getEvents();
					// TODO reset form and get events
				});
			}
		});
	});

	// on click reset
	document.getElementById("eventReset").addEventListener("click", function() {
		const form = this.form;
		lf.checkIfEvents().then(function(val) {
			console.log(val);
			$(".eventExtra").hide();
			$(".eventExtra input").val("");
			$(".row_extraFields").attr("hidden", true);
			$("#eventCategory").selectize()[0].selectize.setValue();
			lp.resetFeedbackFields(form);
		});
	});

	// todo needs confirmation and refresh after clearing
	document.getElementById("clearEvents").addEventListener("click", function() {
		chrome.storage.sync.set({'dataSet' : []}, function() {});
		lf.getEvents();
	});

	new Imgur(document).init();
});
