var lf = lf || {
	checkIfEvents: function() {
		return new Promise(function(resolve, reject) {
			chrome.storage.sync.get(function(dataObj) {
				console.log(dataObj);
				lp.categories = dataObj.categories;
				lp.events = dataObj.dataSet;

				if (dataObj) {
					resolve(dataObj); // resolve data
				} else {
					resolve([]); // resolve Empty array
				}
			});
		}); // eo new Promise
	},
	months: ["december", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november"],
	monthsShort: ["dec", "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov"],
	weekdayArray: ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
	getEvents: function() {
		// no parameters
		lf.checkIfEvents().then(function(val) {
			const dataset = val.dataSet;
			lp.amountEvents = Object.keys(dataset).length;
			lp.fillEditList(dataset);
			lp.events = dataset;
			$("#eventsBadge").html(lp.amountEvents);
			return dataset;
		});
	},
	populateForm: function(form, formDataArray) {
		$.each(formDataArray, function(key, value) {
			$("[name=" + key + "]", form).val(value);
		});
	},
	fillErrors: function(messagesArray) {
		for (key in messagesArray) {
			$("[name='" + key + "_feedback']")
				.html(messagesArray[key])
				.show();
			$("[name='" + key + "']")
				.removeClass("valid")
				.addClass("is-invalid");
		}
	},
	isImdb: function(str) {
		return true; // moet check nog komen
	},
	isCategory: function(str) {
		return true; // moet check nog komen
	},
	isTime: function(str, allowEmpty) {
		if (str === "" && allowEmpty === "true") {
			return true;
		}
		return true; // moet check nog komen
	},
	isDate: function(str) {
		let date = new Date(
			str.split("/")[2] + "-" + str.split("/")[1] + "-" + str.split("/")[0]
		);
		if (isNaN(date.getTime())) {
			console.log('not a valid date')
			return false;
		}
		if (date < new Date()) {
			console.log("date is in the past");
			return false;
		}
		return true; // moet check nog komen
	},
	isURL: function(str) {
		var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var regex = new RegExp(expression);
		return regex.test(str);
	},
	fillMatches: function(value) {

		async function getAllData(competitionId) {
			let dataSet = {};
			await $.ajax({
				url: "http://api.football-data.org/v1/competitions/"+competitionId+"/teams",
				type: "GET",
				headers: {'X-Auth-Token' : '02a710dcd31744f0b7c518ec9cdbac30'},
				dataType: "json",
				async: false,
				cache: false,
				success: function(data) {
					lp.teams[competitionId] = data["teams"];
					dataSet['teams'] = data['teams'];
					return data['teams'];
				}
			});

			await $.ajax({
				url: "http://api.football-data.org/v1/competitions/"+competitionId+"/leagueTable",
				type: "GET",
				headers: {'X-Auth-Token' : '02a710dcd31744f0b7c518ec9cdbac30'},
				dataType: "json",
				async: false,
				cache: false,
				success: function(data) {
					lp.standing[competitionId] = data["standing"];
					dataSet['standing'] = data['standing'];
					return data['standing'];
				}
			});

			await $.ajax({
				url: "http://api.football-data.org/v1/competitions/"+competitionId+"/fixtures",
				type: "GET",
				headers: {'X-Auth-Token' : '02a710dcd31744f0b7c518ec9cdbac30'},
				dataType: "json",
				async: false,
				cache: false,
				success: function(data) {
					lp.fixtures[competitionId] = data["fixtures"];
					dataSet['fixtures'] = data['fixtures'];
					return data['fixtures'];
				}
			});
			dataSet['competitionId'] = competitionId;
			return dataSet;
		}
		return getAllData(value).then((result) => {

			const competitionId = result.competitionId;
			const standing = result.standing;
			let fixtures = result.fixtures;
			const teams = result.teams;

			// build selectize
			document.getElementById('eventExtra_soccer').innerHTML = ''; // reset selectize container
			const selectizeSelect = document.createElement('select');
			selectizeSelect.classList.add('w-100');
			selectizeSelect.id = 'eventExtra_soccer' + competitionId;
			selectizeSelect.name = 'eventExtra_soccer';
			selectizeSelect.placeholder = 'Match';
			document.getElementById('eventExtra_soccer').appendChild(selectizeSelect);

			teamsFiltered = teams.map(function(team) {
				var imageDate = new Image();
				lf.checkIfImageExists(team.crestUrl, function(image) {
					return image ? team.crestUrl : team.crestUrl = false
				});
				return team;
			});

			fixtures = result.fixtures
			.filter(fixture => new Date(fixture.date) > new Date()) // filter fixtures on "in future date"
			.map(function(fixture, idx) {
							let fixtures = {};
							fixture["id"] = idx;
							fixtures = fixture;
							return fixtures;
			}); // provide an id
			const roundsArray = result.fixtures
			.filter(fixture => new Date(fixture.date) > new Date()) // filter fixtures on "in future date"
			.map(function(fixture, idx) {
				let rounds = {};
				rounds["id"] = idx;
				rounds["value"] = fixture.matchday;
				rounds["label"] = "Round: " + fixture.matchday;
				return rounds;
			}); // provide an id, value and label to use as optiongroup

			$("#eventExtra_soccer" + competitionId).selectize({
				valueField: "id",
				maxItems: 1,
				labelField: "homeTeamName",
				searchField: ["awayTeamName", "homeTeamName", "matchday"],
				placeholder: "choose match",
				options: fixtures,
				optgroupField: "matchday",
				optgroups: roundsArray,
				render: {
					item: function(item, escape) {
						const dateObject = new Date(item.date);
						const fullNameDateTime = lf.getFullNameDateTime(dateObject);
						const fullDate = `${fullNameDateTime.monthName} ${fullNameDateTime.day}, ${fullNameDateTime.year} <small>[${fullNameDateTime.time[0]}:${fullNameDateTime.time[1]}]</small>`;
						const weekday = fullNameDateTime.weekdayArray[fullNameDateTime.dayOfWeek];
						const weekdayNr = fullNameDateTime.dayOfWeek;
						let svgHome = "";
						let svgAway = "";
						if (teams !== undefined) {
							svgHome = teams.find(team => team.name === item.homeTeamName);
							svgAway = teams.find(team => team.name === item.awayTeamName);
						}
						let standingAway = { position: false };
						let standingHome = { position: false };
						if (standing !== undefined) {
							standingAway = standing ? standing.find(team => team.teamName === item.awayTeamName) : { position: false };
							standingHome = standing ? standing.find(team => team.teamName === item.homeTeamName) : { position: false };
						}
						const standingHomeDisplay = standingHome.position ? `<b class="selectize_rank">${standingHome.position}</b>` : "";
						const standingAwayDisplay = standingAway.position ? `<b class="selectize_rank">${standingAway.position}</b>` : "";

						const homeSvg = svgHome.crestUrl ? `<img style="width:20px;" src="${svgHome.crestUrl}" alt="${item.homeTeamName}"/> ${item.homeTeamName} ${standingHome ? standingHomeDisplay : ""}` : item.homeTeamName +' '+`${standingHome ? standingHomeDisplay : ""}` ;
						const awaySvg = svgAway.crestUrl ? `<img style="width:20px;" src="${svgAway.crestUrl}" alt="${item.awayTeamName}"/> ${item.awayTeamName} ${standingAway ? standingAwayDisplay : ""}` : item.awayTeamName +' '+`${standingAway ? standingAwayDisplay : ""}`;
						return `<div data-day="${weekdayNr}">
											<span class="selectize_weekday">${weekday}</span>
											<small class="selectize-date">${fullDate}</small>
											<span height="20px">
												${homeSvg} - ${awaySvg}
											</span>
										</div>`;
						},
						optgroup_header: function(data, escape) {
							return `<div class="optgroup-header">Round: ${data.value}</div>`;
						},
						option: function(item, escape) {
							const dateObject = new Date(item.date);
							const fullNameDateTime = lf.getFullNameDateTime(dateObject);
							const fullDate = `${fullNameDateTime.monthName} ${fullNameDateTime.day}, ${fullNameDateTime.year} <small>[${fullNameDateTime.time[0]}:${fullNameDateTime.time[1]}]</small>`;
							const weekday = fullNameDateTime.weekdayArray[fullNameDateTime.dayOfWeek];
							const weekdayNr = fullNameDateTime.dayOfWeek;
							let svgHome = "";
							let svgAway = "";
							if (teams !== undefined) {
								svgHome = teams.find(team => team.name === item.homeTeamName);
								svgAway = teams.find(team => team.name === item.awayTeamName);
							}
							let standingAway = { position: false };
							let standingHome = { position: false };
							if (standing !== undefined) {
								standingAway = standing ? standing.find(team => team.teamName === item.awayTeamName) : { position: false };
								standingHome = standing ? standing.find(team => team.teamName === item.homeTeamName) : { position: false };
							}
							const standingHomeDisplay = standingHome.position ? `<b class="selectize_rank">${standingHome.position}</b>` : "";
							const standingAwayDisplay = standingAway.position ? `<b class="selectize_rank">${standingAway.position}</b>` : "";

							const homeSvg = svgHome.crestUrl ? `<img style="width:20px;" src="${svgHome.crestUrl}" alt="${item.homeTeamName}"/> ${item.homeTeamName}${standingHome ? standingHomeDisplay : ""}` : item.homeTeamName +' '+`${standingHome ? standingHomeDisplay : ""}` ;
							const awaySvg = svgAway.crestUrl ? `<img style="width:20px;" src="${svgAway.crestUrl}" alt="${item.awayTeamName}"/> ${item.awayTeamName}${standingAway ? standingAwayDisplay : ""}` : item.awayTeamName +' '+`${standingAway ? standingAwayDisplay : ""}`;
							return `<div data-day="${weekdayNr}">
												<span class="selectize_weekday">${weekday}</span>
												<small class="selectize-date">${fullDate}</small>
												<span height="20px">
													${homeSvg} - ${awaySvg}
												</span>
											</div>`;

						}
					},
					onChange(value) {
						if (value) {
							const matchArray = this.options[value];
							const name = `${matchArray.homeTeamName} - ${matchArray.awayTeamName}`;
							const searchQuery = `${matchArray.homeTeamName}+${matchArray.awayTeamName}`;
							const dateObject = new Date(matchArray.date);
							const fullNameDateTime = lf.getFullNameDateTime(dateObject);
							const day = ('0' + (String(fullNameDateTime.day))).slice(-2);

							const month = ('0' + (String(fullNameDateTime.month))).slice(-2);
							const year = fullNameDateTime.year;
							const hours = fullNameDateTime.time[0];
							const minutes = fullNameDateTime.time[1] === 0 ? "00" : fullNameDateTime.time[1];
							const datum = `${day}/${month}/${year}`;

							document.querySelector('#eventName').value = name;
							document.querySelector('#eventDate').value = datum;
							document.querySelector('#eventTime').value = `${hours}:${minutes}`;
						} else {
							document.querySelector('#eventName').value = '';
							document.querySelector('#eventDate').value = '';
							document.querySelector('#eventTime').value = '';
						}
					},
					onDropdownOpen() {
						this.clear();
					}
				});
			}); // getAllData();
	},
	checkIfImageExists: function(imageUrl, callBack) {
		if (imageUrl !== null) {
			var imageData = new Image();
			imageData.onload = function() {
			callBack(true);
			};
			imageData.onerror = function() {
			callBack(false);
			};
			imageData.src = imageUrl;
		} else {
			callBack(false);
		}
	},
	getFullNameDateTime: function(dateObject) {
		let fullNameDateTime = {};
		fullNameDateTime['day'] = dateObject.getDate();
		fullNameDateTime['dayOfWeek'] = dateObject.getDay();
		fullNameDateTime['month'] = dateObject.getMonth()+1;
		fullNameDateTime['monthName'] = lf.months[fullNameDateTime['month']];
		fullNameDateTime['year'] = dateObject.getFullYear();
		fullNameDateTime['hours'] = dateObject.getHours();
		fullNameDateTime['minutes'] = dateObject.getMinutes() === 0 ? "00" : dateObject.getMinutes();
		fullNameDateTime['time'] = [fullNameDateTime['hours'], fullNameDateTime['minutes']];
		fullNameDateTime['weekdayArray'] = lf.weekdayArray;

		return fullNameDateTime;
	},
	setMusicSearch: function() {
		$("#eventExtra_music_artists").selectize({
			valueField: "id",
			maxItems: 1,
			labelField: "displayName",
			searchField: "displayName",
			placeholder: "search artist",
			loadThrottle: 300,
			allowEmpty: true,
			options: [],
			onChange(value) {
				console.log(value);
			},
			load: function(query, callback) {
				if (!query.length) return callback();
				$.ajax({
					url: 'http://api.songkick.com/api/3.0/search/artists.json?apikey=f7BntzROcFk22SBa&&query='+ query,
					type: 'GET',
					error: function() {
						callback([]);
					},
					success: function(res) {
						if (!res.resultsPage.results.totalEntries) {
							callback(res.resultsPage.results.artist);
						} else {
							callback([]);
						}
					}
				});
			}
		});
	},
	setImdbSearch: function() {
		$("#eventExtra_imdb").selectize({
			valueField: "Title",
			maxItems: 1,
			labelField: "Title",
			searchField: ["Title"],
			placeholder: "search on movie/serie/game",
			loadThrottle: 300,
			selectOnTab: true,
			allowEmpty: true,
			options: [],
			onChange(value) {
				console.log(value);
			},
			onDropdownOpen(dropdown) {
				this.clear();
			},
			load: function(query, callback) {
				const searchVal = query;
				if (!query.length) return callback();
				$.ajax({
					url: "http://www.omdbapi.com/?s=" + query + "&apikey=af568b9a",
					type: 'GET',
					error: function() {
						callback([]);
					},
					success: function(res) {
						if (res.Response) {
							// const response = res;
							// response.id = searchVal;
							callback(res.Search);
						} else {
							callback([]);
						}
					}
				});

			},
			render: {
				item: function(item, escape) {
					console.log(item);

					let title = item.Title; // display big
					let year = item.Year; // display
					let type = item.Type; // use as small pre
					let poster = '';
					item.Poster === 'N/A' ?  poster = '' : poster = `<img src='${item.Poster}' />`;
					
					return `<div data-type="${type}">
										<span class="selectize_type">${type}</span>
										<span class="selectize_image">
											${poster}
										</span>
										<span class="selectize_content">
											${title}
										</span>
									</div>`
				},
				option: function(item, escape) {
					console.log(item);

					let title = item.Title; // display big
					let year = item.Year; // display
					let type = item.Type; // use as small pre
					let poster = '';
					item.Poster === 'N/A' ?  poster = '' : poster = `<img src='${item.Poster}' />`;
					
					return `<div data-type="${type}">
										<span class="selectize_type">${type}</span>
										<span class="selectize_image">
											${poster}
										</span>
										<span class="selectize_content">
											${title}
										</span>
									</div>`
				}
			}
		});

		// var title = document.getElementById("eventExtra_imdb").value;
		// $("#eventImage_image").html("");
		// $("#eventImage_details").html("");
		// $("#eventImage_details").attr("class", "col-12");
		// $("#eventImage_image").attr("class", "col-hidden");
		// if (title) {
		// 	$.ajax({
		// 		url: "http://www.omdbapi.com/?i=" + title + "&apikey=af568b9a",
		// 		dataType: "json",
		// 		success: function(data) {
		// 			if (data.Error) {
		// 				$("#eventImage_details").html(data.Error);
		// 				$("#eventImage_details").attr("class", "col-12");
		// 				$("#eventImage_image").attr("class", "col-hidden");
		// 				return false;
		// 			}
		// 			var additionalInfo = JSON.stringify(data);
		// 			$("#eventImageAdditional").val(additionalInfo);
		// 			if (data.Poster !== "N/A") {
		// 				$("#eventImage").val(data.Poster);
		// 				$("#eventImage_image").html('<img src="' + data.Poster + '" />');
		// 			}
		// 			$("#eventName").val(data.Title);
		// 			console.log(data);

		// 			if (data.Released) {

		// 			}
		// 			let [Rday, Rmonth, Ryear] = data.Released.split(' ');
		// 			Rday = Rday.length <= 1 ? '0' + Rday : Rday;
		// 			let monthInNumbers = lf.monthsShort.forEach(function (month, index) {
		// 				const parsedMonth = month.toLowerCase()
		// 				const parsedRmonth = Rmonth.toLowerCase()
		// 				if (parsedMonth === parsedRmonth) {
		// 					const monthInNumbers = String(index).length <= 1 ? '0' + index : index;
		// 					$("#eventDate").val(`${Rday}/${monthInNumbers}/${Ryear}`);
		// 				}
		// 			});

		// 			var rating,
		// 				ratingDiv = "";
		// 			if (data.Ratings.length) {
		// 				rating = data.Ratings[0].Value;
		// 				ratingDiv = "<b>IMDB rating: </b>" + rating;
		// 			}
		// 			$("#eventImage_details").html(`
		// 					<b>Title:</b> ${data.Title} <br />
		// 					<b>Genre:</b> ${data.Genre} <br />
		// 					<b>Year:</b> ${data.Year} <br />
		// 					${ratingDiv}
		// 				`);

		// 			if (data.Poster !== "N/A") {
		// 				$("#eventImage_details").attr("class", "col-6");
		// 				$("#eventImage_image").attr("class", "col-6");
		// 			} else {
		// 				$("#eventImage_details").attr("class", "col-12");
		// 				$("#eventImage_image").attr("class", "col-hidden");
		// 			}

		// 			$("#imdb_info").removeClass("hidden");
		// 			$("#eventImageAdditional").attr("name", "imdbData");
		// 			$("#eventImageAdditional").attr("hidden", "false");
		// 		}
		// 	});
		// }


	},
	getCompetitions: function() {
		$.ajax({
			url: "http://api.football-data.org/v1/competitions/",
			type: "GET",
			headers: {'X-Auth-Token' : '02a710dcd31744f0b7c518ec9cdbac30'},
			dataType: "json",
			success: function(data) {
				$("#eventExtra_soccer_competition").selectize({
					valueField: "id",
					maxItems: 1,
					labelField: "caption",
					searchField: ["caption", "id", "league"],
					placeholder: "choose competition",
					options: data,
					onChange(value) {
						if (value) {
							lf.fillMatches(value);
						}
					},
					onDropdownOpen(dropdown) {
						this.clear();
					}
				});
			}
		});
	},
	getArtists: function(artist) {
		// backup api 2bpAQkQkQ3wYFukP
		$.ajax({
			url: "http://api.songkick.com/api/3.0/search/artists.json?apikey=f7BntzROcFk22SBa&&query="+artist,
			type: "GET",
			dataType: "Json",
			success: function(data) {
				console.log(data);

				$.ajax({
					url: "http://api.songkick.com/api/3.0/artists/"+data.resultsPage.results.artist[0].id+"/calendar.json?apikey=f7BntzROcFk22SBa&",
					type: "GET",
					dataType: "Json",
					success: function(data) {
						console.log(data);
					}
				})

			}
		})
	},
	getTeams: function(competitionId) {
		let jsonUrl = "https://cors.io/?http://api.football-data.org/v1/competitions/"+competitionId+"/teams";
		if (competitionId === "449") {
			jsonUrl = "/json/eredivisie_teams.json";
		}
		$.ajax({
			// url: jsonUrl,
			url: "http://api.football-data.org/v1/competitions/"+competitionId+"/teams",
			// url: "/js/champions_league_teams.json",
			// url: jsonUrl,
			type: "GET",
			headers: {'X-Auth-Token' : '02a710dcd31744f0b7c518ec9cdbac30'},
			dataType: "json",
			success: function(data) {
				lp.teams[competitionId] = data["teams"];
			}
		});

		// 	let jsonUrl = "/json/eredivisie_teams.json";
		// 	if (id.competitionId === 464) {
		// 		jsonUrl = "/json/champions_league_teams.json";
		// 	}
		// 	$.ajax({
		// 		// url: "https://cors.io/?http://api.football-data.org/v1/competitions/449/teams",
		// 		// url: "https://cors.io/?http://api.football-data.org/v1/competitions/"+lp.competition+"/teams",
		// 		// url: "/js/champions_league_teams.json",
		// 		url: jsonUrl,
		// 		type: "GET",
		// 		dataType: "json",
		// 		success: function(data) {
		// 			lp.teams[id.competitionId] = data["teams"];
		// 		}
		// 	});
		return competitionId;
	}	
};

var Imgur = function(d) {
	var module = {
		xhr: function() {
			return new XMLHttpRequest();
		},
		create: function(name, props) {
			var el = d.createElement(name),
				p;
			for (p in props) {
				if (props.hasOwnProperty(p)) {
					el[p] = props[p];
				}
			}
			return el;
		},
		remove: function(els) {
			while (els.hasChildNodes()) {
				els.removeChild(els.lastChild);
			}
		},
		bindEvent: function() {
			var fileinput = d.querySelector("#uploadBtn"),
				fileName = d.querySelector("#uploadFile"),
				status = d.querySelector(".status"),
				self = this;

			fileinput.addEventListener("change", function(e) {
					var files = e.target.files, file, div, t, i, len;
					for (i = 0, len = files.length; i < len; i += 1) {
						file = files[i];
						if (file.type.match(/image.*/)) {
							self.remove(status);
							fileName.value = this.value;
							div = self.create("div");
							div.className = "text-variable";
							div.textContent = "Uploading...";
							status.appendChild(div);
							self.upload(file);
						} else {
							self.remove(status);
							div = self.create("div");
							div.textContent = "Invalid Archive";
							status.appendChild(div);
						}
					}
				},
				false
			);
		},
		upload: function(file) {
			var xhttp = module.xhr(),
				status = d.querySelector(".status"),
				self = this,
				fd = new FormData();

			fd.append("image", file);
			xhttp.open("POST", "https://api.imgur.com/3/image");
			xhttp.setRequestHeader("Authorization", "Client-ID 9b1f21201468913"); //Get yout Client ID here: http://api.imgur.com/
			xhttp.onreadystatechange = function() {
				if (xhttp.status === 200 && xhttp.readyState === 4) {
					var res = JSON.parse(xhttp.responseText), link, div, a, t;
					self.remove(status);
					var eventImage = d.querySelector("#eventImage");
					link = res.data.link;
					div = self.create("div");
					div.className = "text-success";
					div.textContent = "successful";
					status.appendChild(div);
					eventImage.value = link;
				}
			};
			xhttp.send(fd);
		},
		init: function() {
			module.bindEvent();
		}
	};

	return {
		init: module.init
	};
};

// {"categories":
// {
// 	"0":{"extraFields":["location","soccer"],"id":0,"name":"sport"},
// 	"1":{"extraFields":["location"],"id":1,"name":"music"},
// 	"2":{"extraFields":["imdb"],"id":2,"name":"television"},
// 	"3":{"extraFields":["location"],"id":3,"name":"personal"}
// }
// }

// {"categories":
// {
// 	"0":{"extraFields":["location","soccer"],"id":0,"name":"sport"},
// 	"1":{"extraFields":["location"],"id":1,"name":"music"},
// 	"2":{"extraFields":["imdb"],"id":2,"name":"television"},
// 	"3":{"extraFields":["location"],"id":3,"name":"personal"},
// 	"4":{"extraFields":["location"],"id":4,"name":"Festival"}
// }
// }

// categories = {"categories":{"0":{"extraFields":["location","soccer"],"id":0,"name":"sport"},"1":{"extraFields":["location"],"id":1,"name":"music"},"2":{"extraFields":["imdb"],"id":2,"name":"television"},"3":{"extraFields":["location"],"id":3,"name":"personal"}}}
// chrome.storage.sync.set({'categories' : categories['categories']}, function() {});

// {"dataSet":[{"eventCategory":"1","eventDate":"28/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://www.paard.nl/wp-content/uploads/2017/02/1482316590racoon_press_2017.jpg","eventName":"Racoon","eventTime":"20:30","eventTime-end":"","uniqueId":0},{"eventCategory":"2","eventDate":"01/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://images-na.ssl-images-amazon.com/images/M/MV5BMzRhYWFkOWMtOGEzZi00M2VhLWJiNTktMjg4ZjAwNGNkOWY3XkEyXkFqcGdeQXVyOTM2ODgzMg@@._V1_UY268_CR43,0,182,268_AL_.jpg","eventName":"De Mol aflevering 2","eventTime":"20:00","eventTime-end":"","uniqueId":1},{"eventCategory":"0","eventDate":"31/03/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"0","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Roda JC","eventTime":"18:30","eventTime-end":"","uniqueId":2},{"eventCategory":"2","eventDate":"06/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"tt6468322","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://ia.media-imdb.com/images/M/MV5BMzBlY2QzNzYtMWU1NS00NjFkLWJiMzItYmM3YTc4MDFjNDQwXkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg","eventName":"seizoen 2 aflevering 1","eventTime":"","eventTime-end":"","imdbData":"{\"Title\":\"Money Heist\",\"Year\":\"2017\",\"Rated\":\"TV-MA\",\"Released\":\"02 May 2017\",\"Runtime\":\"70 min\",\"Genre\":\"Action, Crime, Mystery\",\"Director\":\"N/A\",\"Writer\":\"Álex Pina\",\"Actors\":\"Úrsula Corberó, Itziar Ituño, Álvaro Morte, Paco Tous\",\"Plot\":\"Also known by the international title Money Heist, this limited series crime thriller follows a group of robbers assault on the Factory of Moneda and Timbre.\",\"Language\":\"Russian, Bosnian, Spanish\",\"Country\":\"Spain\",\"Awards\":\"3 wins & 12 nominations.\",\"Poster\":\"https://ia.media-imdb.com/images/M/MV5BMzBlY2QzNzYtMWU1NS00NjFkLWJiMzItYmM3YTc4MDFjNDQwXkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.8/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"8.8\",\"imdbVotes\":\"32,118\",\"imdbID\":\"tt6468322\",\"Type\":\"series\",\"totalSeasons\":\"1\",\"Response\":\"True\"}","uniqueId":3},{"eventCategory":"0","eventDate":"07/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"10","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Nac - Vitesse","eventTime":"18:30","eventTime-end":"","uniqueId":4},{"eventCategory":"0","eventDate":"14/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"20","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Sparta","eventTime":"19:45","eventTime-end":"","uniqueId":5},{"eventCategory":"0","eventDate":"18/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"30","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"AZ Alkmaar - Vitesse","eventTime":"18:30","eventTime-end":"","uniqueId":6},{"eventCategory":"0","eventDate":"29/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"42","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Fc twente","eventTime":"14:30","eventTime-end":"","uniqueId":7},{"eventCategory":"0","eventDate":"06/05/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"52","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Willem II - Vitesse","eventTime":"14:30","eventTime-end":"","uniqueId":8},{"eventCategory":"3","eventDate":"26/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"http://wallpoper.com/images/00/06/35/06/t-mobile_00063506.jpg","eventName":"T-mobile telefoon verlengen","eventTime":"","eventTime-end":"","uniqueId":9},{"eventCategory":"3","eventDate":"24/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://www.pluimen.nl/sites/default/files/product-images/thermen-bussloo_6.jpg","eventName":"Bussloo Sauna","eventTime":"14:30","eventTime-end":"","uniqueId":10}]}


// dataset = {"dataSet":[{"eventCategory":"1","eventDate":"28/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://www.paard.nl/wp-content/uploads/2017/02/1482316590racoon_press_2017.jpg","eventName":"Racoon","eventTime":"20:30","eventTime-end":"","uniqueId":0},{"eventCategory":"2","eventDate":"01/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://images-na.ssl-images-amazon.com/images/M/MV5BMzRhYWFkOWMtOGEzZi00M2VhLWJiNTktMjg4ZjAwNGNkOWY3XkEyXkFqcGdeQXVyOTM2ODgzMg@@._V1_UY268_CR43,0,182,268_AL_.jpg","eventName":"De Mol aflevering 2","eventTime":"20:00","eventTime-end":"","uniqueId":1},{"eventCategory":"0","eventDate":"31/03/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"0","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Roda JC","eventTime":"18:30","eventTime-end":"","uniqueId":2},{"eventCategory":"2","eventDate":"06/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"tt6468322","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://ia.media-imdb.com/images/M/MV5BMzBlY2QzNzYtMWU1NS00NjFkLWJiMzItYmM3YTc4MDFjNDQwXkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg","eventName":"seizoen 2 aflevering 1","eventTime":"","eventTime-end":"","imdbData":"{\"Title\":\"Money Heist\",\"Year\":\"2017\",\"Rated\":\"TV-MA\",\"Released\":\"02 May 2017\",\"Runtime\":\"70 min\",\"Genre\":\"Action, Crime, Mystery\",\"Director\":\"N/A\",\"Writer\":\"Álex Pina\",\"Actors\":\"Úrsula Corberó, Itziar Ituño, Álvaro Morte, Paco Tous\",\"Plot\":\"Also known by the international title Money Heist, this limited series crime thriller follows a group of robbers assault on the Factory of Moneda and Timbre.\",\"Language\":\"Russian, Bosnian, Spanish\",\"Country\":\"Spain\",\"Awards\":\"3 wins & 12 nominations.\",\"Poster\":\"https://ia.media-imdb.com/images/M/MV5BMzBlY2QzNzYtMWU1NS00NjFkLWJiMzItYmM3YTc4MDFjNDQwXkEyXkFqcGdeQXVyMTA0MjU0Ng@@._V1_SX300.jpg\",\"Ratings\":[{\"Source\":\"Internet Movie Database\",\"Value\":\"8.8/10\"}],\"Metascore\":\"N/A\",\"imdbRating\":\"8.8\",\"imdbVotes\":\"32,118\",\"imdbID\":\"tt6468322\",\"Type\":\"series\",\"totalSeasons\":\"1\",\"Response\":\"True\"}","uniqueId":3},{"eventCategory":"0","eventDate":"07/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"10","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Nac - Vitesse","eventTime":"18:30","eventTime-end":"","uniqueId":4},{"eventCategory":"0","eventDate":"14/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"20","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Sparta","eventTime":"19:45","eventTime-end":"","uniqueId":5},{"eventCategory":"0","eventDate":"18/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"30","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"AZ Alkmaar - Vitesse","eventTime":"18:30","eventTime-end":"","uniqueId":6},{"eventCategory":"0","eventDate":"29/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"42","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Vitesse - Fc twente","eventTime":"14:30","eventTime-end":"","uniqueId":7},{"eventCategory":"0","eventDate":"06/05/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"52","eventExtra_soccer2":"","eventImage":"https://adn.fcupdate.nl/news/85725.jpg","eventName":"Willem II - Vitesse","eventTime":"14:30","eventTime-end":"","uniqueId":8},{"eventCategory":"3","eventDate":"26/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"http://wallpoper.com/images/00/06/35/06/t-mobile_00063506.jpg","eventName":"T-mobile telefoon verlengen","eventTime":"","eventTime-end":"","uniqueId":9},{"eventCategory":"3","eventDate":"24/04/2018","eventDate-end":"","eventDescription":"","eventExtra_imdb":"","eventExtra_location":"","eventExtra_soccer":"","eventExtra_soccer2":"","eventImage":"https://www.pluimen.nl/sites/default/files/product-images/thermen-bussloo_6.jpg","eventName":"Bussloo Sauna","eventTime":"14:30","eventTime-end":"","uniqueId":10}]}
// chrome.storage.sync.set({'dataSet' : dataset['dataSet']}, function() {});

