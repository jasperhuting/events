# Events

## WIP / TODO

> Options page
---
- Finish: music api
- Add: validation on submit
	- time
	- date
	- imdb code
- Fix: Delete all events -> popup and refresh after deleting
- Fix: Add Category color to categories list
- Fix: Clean up code/logic

> Popup page
---

- Fix: Scroll to first future event
- Fix: Styling event card
- Fix: Add read more on description
- Add: Option panel with categories
- Add: Add edit button that opens options page and fill form
- Add: Clean up code/logic

> Extra
---

- Add: Add css3 animations where needed
- Add: Add api for personal
- Add: Add icons per category
- Add: Add user info
- Add: Add tab where you can watch events on options page
- Add: Add notifications
- Add: Add badge with amount events
- Add: Add languages + language switch
- Add: Add datepicker
- Add: Add extra fields if needed


## Table of Contents (Optional)

> If you're `README` has a lot of info, section headers might be nice.

- [Installation](#installation)
- [FAQ](#faq)
- [Support](#support)
- [License](#license)
- [Donations](#donations)


---

## Installation

- Download and place in a map on your pc
- Start The browser Google Chrome and click on settings
- Click on more tools
- Click on extensions
- Click on LOAD UNPACKED
- and choose the folder where you placed the event extension

### Clone

- Clone this repo to your local machine using `https://gitlab.com/jasperhuting/events.git`

## Usage (Optional)
## Documentation (Optional)
## FAQ

- **How can i use this awesome tool?**
    - It's really easy! Look at the installation part and if you don't get it after that contact me!

---

## Support

Reach out to me at one of the following places!

- Website at <a href="http://www.jasperhuting.nl" target="_blank">`jasperhuting.nl`</a>
- Insert more social links here.

---

## Donations (Optional)


---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2018 © <a href="http://jasperhuting.nl" target="_blank">Jasperhuting</a>.